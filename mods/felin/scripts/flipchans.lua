function init()
	if storage.channel == nil then
		storage.channel = config.getParameter("defaultChannel", 0)
	end
	self.interactive = config.getParameter("interactive", true)
	object.setInteractive(self.interactive)
	setChannel(storage.channel)
	
	message.setHandler("flip", function(_, _)
		flipChannels()
	end)
end

function onInteraction(args)
	if not config.getParameter("inputNodes") or not object.isInputNodeConnected(0) then
		flipChannels()
	end
end

function setChannel(channel)
	if config.getParameter("channelDescriptions", nil) then
		local descs = config.getParameter("channelDescriptions", nil)[channel + 1]
		for k,v in pairs(descs) do
			object.setConfigParameter(k, v)
		end
	end
	if channel > 0 then
		-- animator.setGlobalTag("channel", channel);
		animator.setAnimationState("tv", "channel" .. channel)
		object.setSoundEffectEnabled(true)
		if animator.hasSound("on") then
			animator.playSound("on")
		end
		object.setLightColor(config.getParameter("lightColor", {255, 255, 255}))
	else
		animator.setAnimationState("tv", "off")
		object.setSoundEffectEnabled(false)
		if animator.hasSound("off") then
			animator.playSound("off")
		end
		object.setLightColor(config.getParameter("lightColorOff", {0, 0, 0}))
	end
end

function flipChannels()
	local channels = config.getParameter("numChannels", 1) + 1
	local nextChannel = storage.channel + 1
	if nextChannel == channels then nextChannel = 0 end
	storage.channel = nextChannel
	setChannel(storage.channel)
	return true
end

